import {createStackNavigator} from '@react-navigation/stack' 
import { NavigationContainer } from '@react-navigation/native';
import Search from '../components/Search'
import FilmDetails from '../components/FilmDetails'
import React, {Fragment} from 'react';

const Stack = createStackNavigator();

function SearchStackNavigator() {
  return (
    <NavigationContainer>
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen
        name="Search"
        component={Search}
        options={{ title: 'Search Films' }}
      />
      <Stack.Screen
        name="FilmDetails"
        component={FilmDetails}
        options={{ title: 'Films Details' }}
      />
    </Stack.Navigator>
    </NavigationContainer>
  );
}
export default SearchStackNavigator