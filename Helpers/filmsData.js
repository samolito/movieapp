	
export default data=[
    {
    popularity:	204.378,
    vote_count:	2146,
    video:	false,
    poster_path	:"https://iatse514.com/wp-content/uploads/2020/03/bloodshot-posterresize.jpg",
    id	:338762,
    adult:	false,
    backdrop_path:	"/ocUrMYbdjknu2TwzMHKT9PBBQRw.jpg",
    original_language:	"en",
    original_title:	"Bloodshot",
    genre_ids:[	
        28,
        878],
    title:	"Bloodshot",
    vote_average:	7.1,
    overview:	"After he and his wife are murdered, marine Ray Garrison is resurrected by a team of scientists. Enhanced with nanotechnology, he becomes a superhuman, biotech killing machine—'Bloodshot'. As Ray first trains with fellow super-soldiers, he cannot recall anything from his former life. But when his memories flood back and he remembers the man that killed both him and his wife, he breaks out of the facility to get revenge, only to discover that there's more to the conspiracy than he thought.",
    release_date:	"2020-03-05"
    },{
        
    popularity:	188.362,
    vote_count:	924,
    video:	false,
    poster_path:	"https://m.media-amazon.com/images/M/MV5BZDA1ZmQ2OGMtZDhkMC00ZjRkLWE3ZTMtMzA5ZTk0YjM1OGRmXkEyXkFqcGdeQXVyNzI1NzMxNzM@._V1_.jpg",
    id	:481848,
    adult:	false,
    backdrop_path:	"/9sXHqZTet3Zg5tgcc0hCDo8Tn35.jpg",
    original_language:	"en",
    original_title:	"The Call of the Wild",
    genre_ids:[
        12,
        18,
        10751],
    title:	"The Call of the Wild",
    vote_average:	7.3,
    overview:	"Buck is a big-hearted dog whose blissful domestic life is turned upside down when he is suddenly uprooted from his California home and transplanted to the exotic wilds of the Yukon during the Gold Rush of the 1890s. As the newest rookie on a mail delivery dog sled team—and later its leader—Buck experiences the adventure of a lifetime, ultimately finding his true place in the world and becoming his own master.",
    release_date:	"2020-02-19",
    },
    {
    
    popularity:	170.924,
    vote_count:	270,
    video:	false,
    poster_path:	"https://image.tmdb.org/t/p/w600_and_h900_bestv2/a1Wc96TasY2rNhDpt3kjbum2nSd.jpg",
    id:	618344,
    adult:	false,
    backdrop_path	:"/sQkRiQo3nLrQYMXZodDjNUJKHZV.jpg",
    original_language:	"en",
    original_title:	"Justice League Dark: Apokolips War",
    genre_ids:[
        28,
        12,
        16,
        14,
        878,
    ],
    title:	"Justice League Dark: Apokolips War",
    vote_average:	8.8,
    overview:	"Earth is decimated after intergalactic tyrant Darkseid has devastated the Justice League in a poorly executed war by the DC Super Heroes. Now the remaining bastions of good – the Justice League, Teen Titans, Suicide Squad and assorted others – must regroup, strategize and take the war to Darkseid in order to save the planet and its surviving inhabitants.",
    release_date:	"2020-05-05",
    },
        
    {
    popularity:	185.116,
    vote_count:	3740,
    video:	false,
    poster_path:	"https://upload.wikimedia.org/wikipedia/en/c/c1/Sonic_the_Hedgehog_poster.jpg",
    id:	454626,
    adult:	false,
    backdrop_path:	"/stmYfCUGd8Iy6kAMBr6AmWqx8Bq.jpg",
    original_language:	"en",
    original_title:	"Sonic the Hedgehog",
    genre_ids	:
    [	28,
        35,
        878,
        10751],
    title:	"Sonicthe Hedgehog",
    vote_average:	7.6,
    overview:	"Based on the global blockbuster videogame franchise from Sega, Sonic the Hedgehog tells the story of the world’s speediest hedgehog as he embraces his new home on Earth. In this live-action adventure comedy, Sonic and his new best friend team up to defend the planet from the evil genius Dr. Robotnik and his plans for world domination.",
    release_date:	"2020-02-12"
    },	
    {
    popularity:	126.935,
    vote_count:	7141,
    video	:false,
    poster_path:	"https://miro.medium.com/max/897/1*y4xha6Kpk_mFOlNA-1GzTQ.jpeg",
    id	:496243,
    adult:	false,
    backdrop_path:	"/ApiBzeaa95TNYliSbQ8pJv4Fje7.jpg",
    original_language:	"ko",
    original_title:	"기생충",
    genre_ids	:
    [	35,
        18,
        53],
    title:	"Parasite",
    vote_average:	8.5,
    overview:	"All unemployed, Ki-taek's family takes peculiar interest in the wealthy and glamorous Parks for their livelihood until they get entangled in an unexpected incident.",
    release_date:	"2019-05-30"},
    
    {
            popularity:	83.05,
            vote_count:	657,
            video:	false,
            poster_path:	"https://images-na.ssl-images-amazon.com/images/I/912UPJuKy8L._RI_.jpg",
            id:	539537,
            adult:	false,
            backdrop_path:	"/x80ZIVGUJ6plcUBcgVZ6DPKT7vc.jpg",
            original_language:	"en",
            original_title:	"Fantasy Island",
            genre_ids:
            [	14,
                27,
                878],
            title:	"Fantasy Island",
            vote_average:	6.1,
            overview:	"A group of contest winners arrive at an island hotel to live out their dreams, only to find themselves trapped in nightmare scenarios.",
            release_date:	"2020-02-12"
    },	
        {popularity:	71.73,
        vote_count:	1659,
        video:	false,
        poster_path:	"https://lumiere-a.akamaihd.net/v1/images/p_onward_19024_dbb512d5.jpeg",
        id:	508439,
        adult:	false,
        backdrop_path:	"/xFxk4vnirOtUxpOEWgA1MCRfy6J.jpg",
        original_language:	"en",
        original_title:	"Onward",
        genre_ids:	
        [	12,
            16,
            35,
            14,
            10751],
        title:	"Onward",
        vote_average:	7.9,
        overview:	"In a suburban fantasy world, two teenage elf brothers embark on an extraordinary quest to discover if there is still a little magic left out there.",
        release_date:	"2020-02-29"
    },{	
    popularity:	66.873,
    vote_count:	237,
    video:	false,
    poster_path:	"https://cdn.iphoneincanada.ca/wp-content/uploads/2020/04/my-spy-poster.jpg",
    id:	592834,
    adult:	false,
    backdrop_path:	"/yalJdTsb6EcDX5devj2ltWXuceO.jpg",
    original_language:	"en",
    original_title:	"My Spy",
    genre_ids:	
    [	28,
        35,
        10751,],
    title:	"My Spy",
    vote_average:	7,
    overview:	"A hardened CIA operative finds himself at the mercy of a precocious 9-year-old girl, having been sent undercover to surveil her family.",
    release_date:	"2020-01-09",
    },
    {
    popularity:	67.717,
    vote_count:	622,
    video:	false,
    poster_path:	"https://m.media-amazon.com/images/M/MV5BNWE5ZjUyNTQtNzY5NC00YjRkLTk3M2QtMTRhOTdjYWFhNDZjXkEyXkFqcGdeQXVyNzcyMzY5NA@@._V1_.jpg",
    id:	514847,
    adult:	false,
    backdrop_path:	"/qfQ78ZKiouoM2yhAnfNblp9ijQE.jpg",
    original_language:	"en",
    original_title:	"The Hunt",
    genre_ids:	
    [	28,
    27,
        53],
    title:	"The Hunt",
    vote_average:	6.8,
    overview:	"Twelve strangers wake up in a clearing. They don't know where they are—or how they got there. In the shadow of a dark internet conspiracy theory, ruthless elitists gather at a remote location to hunt humans for sport. But their master plan is about to be derailed when one of the hunted turns the tables on her pursuers.",
    release_date:	"2020-03-11"
    },
    {
    popularity:	68.192,
    vote_count:	1129,
    video:	false,
    poster_path:	"https://upload.wikimedia.org/wikipedia/en/0/06/The_Gentlemen_poster.jpg",
    id:	522627,
    adult:	false,
    backdrop_path:	"/tintsaQ0WLzZsTMkTiqtMB3rfc8.jpg",
    original_language:	"en",
    original_title:	"The Gentlemen",
    genre_ids:	
    [	28,
        35,
        80],
    title:	"The Gentlemen",
    vote_average:	7.7,
    overview:	"American expat Mickey Pearson has built a highly profitable marijuana empire in London. When word gets out that he’s looking to cash out of the business forever it triggers plots, schemes, bribery and blackmail in an attempt to steal his domain out from under him.",
    release_date:	"2020-01-01"
    },
    
    {
    popularity:	68.822,
    vote_count:	245,
    video:	false,
    poster_path:	"https://upload.wikimedia.org/wikipedia/en/5/53/Emma_poster.jpeg",
    id:	556678,
    adult:	false,
    backdrop_path:	"/5GbkL9DDRzq3A21nR7Gkv6cFGjq.jpg",
    original_language:	"en",
    original_title:	"Emma.",
    genre_ids:
    [	35,
        18,
        10749],
    title:	"Emma.",
    vote_average:	7.1,
    overview:	"In 1800s England, a well-meaning but selfish young woman meddles in the love lives of her friends.",
    release_date:	"2020-02-13"
    
    }
    ]