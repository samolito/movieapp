/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import Search from './components/Search';
import Navigator from './Navigation/Navigation'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const App = () => {
  return (
    <Navigator/>
    // <View  style={styles.container} >
    //  <Search/>
    // </View>
  );
};

const styles = StyleSheet.create({
  container:{
    flex:1,
 },
});

export default App;
