import React from 'react'
import {Image, View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import {getImagefromAPI} from '../API/TMDApi'
class FilmsItem extends React.Component{
    
    render(){
        const film = this.props.film
        const displayDetailsForFilm = this.props.displayDetailsForFilm
       return(
           <TouchableOpacity 
           onPress={()=>displayDetailsForFilm(film.title)}
           style={styles.mainContainer}>
               <View style={styles.imagecontainer}> 
                   <Image source={{uri: getImagefromAPI( film.poster_path)}}
                    style={{width: 120, height: 180}} />
                </View>

                <View style={styles.content_container}> 
                    <View style={styles.header_container}>
                       <Text style={styles.text_title}>{film.title}</Text>
                        <Text style={styles.text_vote}>{film.vote_count}</Text>
                    </View>
                    <View style={styles.descrip_container}>
                    <Text style={styles.text_desc} numberOfLines={5}> {film.overview}</Text>
                    </View>
                    <View style={styles.date_container}>
                    <Text style={styles.text_date}> Date release {film.release_date}</Text>
                    </View>

                    
                </View>
           </TouchableOpacity>
       )
    }
}
const styles= StyleSheet.create({
    mainContainer:{
        margin:5,
        flexDirection:'row',
        height:180
     },
    imagecontainer:{
       flex:2,
       backgroundColor:'white'
    },
    content_container:{
       flex:4,
        backgroundColor:'white',
     },
     header_container:{
        flex:2,
        flexDirection:'row',
        backgroundColor:'white',
     },
     descrip_container:{
        flex:4,
     },
     date_container:{
        flex:1,
     },
    
     text_title: {
        color: 'red',
        fontSize:20,
        flex:1,
        flexWrap:'wrap',
        fontWeight:'bold'
      },
      text_vote: {
        color: 'red',
        fontSize:18,
        fontWeight:'bold'
    
      },
      text_desc: {
        color: 'red',
        fontSize:14,
        fontWeight:'900'
      },
      text_date: {
        color: 'red',
        fontSize:15,
        fontWeight:'200',
        textAlign:'right',
        marginRight:10
      },
});

export default FilmsItem