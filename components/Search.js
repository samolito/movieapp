import React from 'react'
import {View, TextInput, Button, StyleSheet, FlatList, ActivityIndicator} from 'react-native'
import films from '../Helpers/filmsData'
import FilmItem from './FilmItem'
import { getfilmsfromAPI } from '../API/TMDApi'
class Search extends React.Component{

constructor(props){
    super(props)
    this.state = {films: [],
    isLoading :false
    };
    this.searchedText=""
 //   this._loadfilms = this._loadfilms.bind(this)
}
    _loadfilms(){
        this.setState({isLoading:true})
        if(this.state.searchedText.length > 0 ){
            //alert(this.state.isLoading)
             getfilmsfromAPI(this.state.searchedText).then(data =>
                this.setState({
                    films:data.results,
                    isLoading:false
            }))
      
        }
    }
    _displayloading(){
        if(this.state.isLoading){
        return(
        <View style={styles.loading_container}>
            <ActivityIndicator size='large' color='blue'/>
        </View>
        )
        }
    }
    _searchTextInputChanged(text){
    this.setState({searchedText:text })
    }
    _displayDetailsForFilm= (idfilm)=>{
        //alert('id Film :'+idfilm)
        this.props.navigation.navigate("FilmDetails", {'idfilm': idfilm});
    } 
    render(){   
            return(
               <View style={styles.container}>
                  <TextInput onSubmitEditing={()=>this._loadfilms()} onChangeText={(text)=>this._searchTextInputChanged(text)} style={styles.textInput} placeholder="Titre du film"/>
                   <Button style={styles.button} title="Search" onPress={()=>{}}/>
                   <FlatList
                data={this.state.films}
               keyExtractor={(item) =>item.title.toString()}
                renderItem={({item}) => <FilmItem film={item} displayDetailsForFilm={this._displayDetailsForFilm}/>}/>
                
                 {this._displayloading()}      
               </View>
              
            )
            
        }
    }
    const styles= StyleSheet.create({
        container:{
           flex:1,
           marginLeft:10,
           marginRight:10
        },
        textInput:{
           margin:10,
           height:40,
           borderColor:'skyblue',
           borderWidth:2
         },
         button:{
             marginLeft:30,
             marginRight:30,
             borderRadius:5
         },
         loading_container:{
             position:'absolute',
             top:100,
             right:0,
             left:0,
             bottom:0,
             alignItems:'center',
             justifyContent:'center'
         }
         
      
    });
    
    export default Search